const { getSurroundingPositions, invalidTerrain, getParts } = require('./utility')
const tower = require('./structure.tower')

const buildExtensions = ({ room, spawns, terrain }) => {
  const { level } = room.controller
  const { extensions, extensionCount } = room.memory
  if (level < 2) { return }

  const { x, y } = spawns[0].pos
  const extensionsToBuild = Memory.extensionCount[level - 2]

  let numberOfExtensions = extensionCount || 0

  if (extensionsToBuild !== (extensions || []).length) {
    room.memory.extensionMapped = false
  }

  if (numberOfExtensions === extensionsToBuild) {
    if (!room.memory.parts) {
      room.memory.parts = {}
      for (const type in Memory.parts) {
        room.memory.parts[type] = getParts(room, type)
      }
    }
    return
  }

  for (let i = level; i < (level + 1 < 9 ? level + 1 : 9); i++) {
    const positions = getSurroundingPositions(x, y, i)
    for (const [x, y] of positions) {
      if (!invalidTerrain(terrain, x, y) && room.lookForAt(LOOK_STRUCTURES, x, y).length === 0) {
        if (room.createConstructionSite(x, y, STRUCTURE_EXTENSION) === OK) {
          numberOfExtensions++
        }
      }
      if (numberOfExtensions === extensionsToBuild) { break }
    }
    if (numberOfExtensions === extensionsToBuild) { break }
  }

  room.memory.extensionCount = extensionsToBuild
}

const buildRoads = ({ room }) => {
  const roads = Object.keys(room.memory.roads)
  for (const road of roads) {
    const [x, y] = road.split(',')
    if (room.lookForAt(LOOK_STRUCTURES, x, y).filter(s => s.structureType === STRUCTURE_ROAD).length === 0) {
      room.createConstructionSite(x, y, STRUCTURE_ROAD)
    }
  }
}

const updateSources = ({ room }) => {
  const { level } = room.controller
  let { controllerLevel } = room.memory

  if (level < 3) {
    return
  }

  if (!controllerLevel) {
    for (const id in room.memory.sources) {
      room.memory.sources[id].maxHarvesters += level - 1
      room.memory.maxHarvesters += level - 1
    }
  } else if (controllerLevel > level) {
    // controller downgraded!
    for (const id in room.memory.sources) {
      room.memory.sources[id].maxHarvesters -= controllerLevel - level
      room.memory.maxHarvesters -= controllerLevel - level
    }
  } else {
    // controller upgraded!
    const { sources } = room.memory
    for (const id in sources) {
      room.memory.sources[id].maxHarvesters += controllerLevel - level
      room.memory.maxHarvesters += controllerLevel - level
    }
  }

  room.memory.controllerLevel = level
}

const buildTowers = ({ room, terrain }) => {
  const { level, pos } = room.controller
  const { x, y } = pos
  if (level < 3) {
    return
  }
  let towerCount = room.memory.towerCount || 0
  const towersToBuild = Memory.towerCount[level - 3]

  if (towerCount === towersToBuild) {
    return
  }

  for (let i = 1; i < (level + 1 < 8 ? level + 1 : 8); i++) {
    const positions = getSurroundingPositions(x, y, i)
    for (const [x, y] of positions) {
      if (!invalidTerrain(terrain, x, y)) {
        if (room.createConstructionSite(x, y, STRUCTURE_TOWER) === OK) {
          towerCount++
        } else {
          continue
        }
      }
      if (towerCount === towersToBuild) { break }
    }
    if (towerCount === towersToBuild) { break }
  }

  room.memory.towerCount = towersToBuild
}

const manageStructures = ({ room, spawns, terrain }) => {
  buildExtensions({ room, spawns, terrain })
  buildRoads({ room })
  buildTowers({ room, terrain })
  updateSources({ room })

  // remap construction sites
  const constructionSites = room.find(FIND_CONSTRUCTION_SITES).map(site => site.id)
  room.memory.constructionSites = constructionSites

  // tower strategy
  const towers = room.find(FIND_MY_STRUCTURES, {
    filter: s => s.structureType === STRUCTURE_TOWER
  })

  if (towers) {
    for (const t of towers) {
      tower.use(t)
    }
  }
}

module.exports = manageStructures
