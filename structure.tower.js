const use = tower => {
  if ((tower.room.memory.hostile || []).length > 0) {
    let creepToHeal = null
    for (const creep of _.filter(Game.creeps, c => c.memory.role = Memory.roles.defender)) {
      if (creep.hits < creep.hitsMax) {
        creepToHeal = creep
        break
      }
    }
    if (creepToHeal) {
      tower.heal(creepToHeal)
    } else {
      tower.attack(Game.getObjectbyId(creep.room.memory.hostile[0]))
    }
  }
}

module.exports = {
  use
}