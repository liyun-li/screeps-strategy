const use = creep => {
  if (!Game.creeps[creep.name]) {
    return
  }

  const { building } = creep.memory

  if (building && creep.store[RESOURCE_ENERGY] === 0) {
    creep.memory.building = false
  } else if (!building && creep.store[RESOURCE_ENERGY] === creep.store.getCapacity(RESOURCE_ENERGY)) {
    creep.memory.building = true
  }

  if (creep.memory.building) {
    // go build something
    let constructionSite
    if (!creep.memory.siteId) {
      if (creep.room.memory.constructionSites.length === 0) {
        return
      }
      const sites = creep.room.memory.constructionSites.map(id => Game.getObjectById(id))
      constructionSite = creep.pos.findClosestByPath(sites)
      creep.memory.siteId = constructionSite.id
    } else {
      constructionSite = Game.getObjectById(creep.memory.siteId)
    }

    const buildStatus = creep.build(constructionSite)

    if (buildStatus === ERR_NOT_IN_RANGE) {
      creep.moveTo(constructionSite, { visualizePathStyle: { stroke: '#ffffff' } })
    } else if (buildStatus === ERR_INVALID_TARGET) {
      creep.memory.siteId = null
    } else if (buildStatus !== OK) {
      creep.say(`Error ${buildStatus}`)
    }
  } else {
    // go get some energy
    const { energySourceId } = creep.memory
    let energySource = null

    if (!energySourceId) {
      energySource = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
        filter: s => {
          const hasEnergy = !!s.store && s.store[RESOURCE_ENERGY] > 0

          return hasEnergy && [
            STRUCTURE_SPAWN,
            STRUCTURE_EXTENSION,
            STRUCTURE_CONTAINER,
          ].includes(s.structureType)
        }
      })

      if (!energySource) {
        energySource = creep.pos.findClosestByPath(FIND_RUINS, {
          filter: ruin => ruin.store[RESOURCE_ENERGY] > 0
        })
      }

    } else {
      energySource = Game.getObjectById(energySourceId)
    }

    if (!energySource) { return }

    creep.memory.energySourceId = energySource.id
    const result = creep.withdraw(energySource, RESOURCE_ENERGY)
    if (result === ERR_NOT_IN_RANGE) {
      creep.moveTo(energySource, { visualizePathStyle: { stroke: '#ffffff' } })
    } else if (result === ERR_NOT_ENOUGH_RESOURCES) {
      creep.memory.energySourceId = creep.pos.findClosestByPath(FIND_MY_STRUCTURES, {
        filter: s => {
          const hasEnergy = !!s.store && s.store[RESOURCE_ENERGY] > 0

          return hasEnergy && [
            STRUCTURE_SPAWN,
            STRUCTURE_EXTENSION,
            STRUCTURE_CONTAINER,
          ].includes(s.structureType)
        }
      }).id
    }
  }
}

module.exports = {
  use
}
