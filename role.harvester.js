const use = creep => {
  if (!Game.creeps[creep.name]) {
    return
  }

  let sourceId = creep.memory.sourceId

  if (!sourceId) {
    for (const id in creep.room.memory.sources) {
      const data = creep.room.memory.sources[id]
      if (data.harvesters < data.maxHarvesters) {
        sourceId = id
        creep.memory.sourceId = id
        creep.room.memory.sources[id].harvesters++
        break
      }
    }
  }

  // redundant creeps become builders
  if (!sourceId) {
    creep.memory.role = Memory.roles.builder
    return
  }

  const source = Game.getObjectById(sourceId)
  if (creep.store[RESOURCE_ENERGY] < creep.store.getCapacity(RESOURCE_ENERGY)) {
    if (creep.harvest(source) === ERR_NOT_IN_RANGE) {
      creep.moveTo(source, {
        visualizePathStyle: { stroke: '#ffaa00' }
      })
    }
  } else {
    const closestRepository = creep.pos.findClosestByPath(
      FIND_MY_STRUCTURES,
      {
        filter: s => {
          const hasFreeCapacity = s.store && s.store.getFreeCapacity(RESOURCE_ENERGY) > 0

          return hasFreeCapacity && [
            STRUCTURE_SPAWN,
            STRUCTURE_EXTENSION,
            STRUCTURE_CONTAINER
          ].includes(s.structureType)
        }
      }
    )

    if (creep.transfer(closestRepository, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
      creep.moveTo(closestRepository, { visualizePathStyle: { stroke: '#ffffff' } })
    }
  }

  const { x, y } = creep.pos
  if (creep.pos.createConstructionSite(STRUCTURE_ROAD) === OK) {
    creep.room.memory.roads[`${x},${y}`] = 1
  }
}

module.exports = {
  use
}