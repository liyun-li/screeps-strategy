const { spawnCreep, getParts } = require('./utility')
const harvester = require('./role.harvester')
const builder = require('./role.builder')
const upgrader = require('./role.upgrader')
const defender = require('./role.defender')

const manageCreeps = ({ room, spawns }) => {
  const harvesters = []
  const builders = []
  const upgraders = []
  const defenders = []

  for (const name in Memory.creeps) {
    const creep = Game.creeps[name]
    if (!creep) {
      // Delete non-existent creeps from memory
      if (Memory.creeps[name].sourceId) {
        room.memory.sources[Memory.creeps[name].sourceId].harvesters -= 1
      }
      delete Memory.creeps[name]
    } else {
      // Categorize the creeps
      if (creep.memory.role === Memory.roles.harvester) {
        harvesters.push(creep)
        harvester.use(creep)
      } else if (creep.memory.role === Memory.roles.builder) {
        builders.push(creep)
        builder.use(creep)
      } else if (creep.memory.role === Memory.roles.upgrader) {
        upgraders.push(creep)
        upgrader.use(creep)
      } else if (creep.memory.role === Memory.roles.defender) {
        defenders.push(creep)
        defender.use(creep)
      }
    }
  }

  const constructionSites = room.find(FIND_MY_CONSTRUCTION_SITES)
  if (constructionSites.length > 0) {
    // if there are things to build again
    // upgraders that used to be builders will become builders again
    for (const creep of upgraders.filter(creep => creep.memory.building !== undefined)) {
      creep.memory.role = Memory.roles.builder
    }
    room.memory.constructionSites = constructionSites.map(site => site.id)
  } else {
    // if no construction sites are available
    // convert all builders to upgraders
    for (const creep of builders) {
      creep.memory.role = Memory.roles.upgrader
    }
  }

  if (harvesters.length < room.memory.maxHarvesters) {
    // convert some builders and upgraders into harvesters
    let harvestersNeeded = room.memory.maxHarvesters - harvesters.length
    const notEnoughBuilders = harvestersNeeded > builders.length
    for (let i = 0; i < (notEnoughBuilders ? builders.length : harvestersNeeded); i++) {
      const creep = builders.pop()
      creep.memory.role = Memory.roles.harvester
    }
    if (notEnoughBuilders) {
      harvestersNeeded -= builders.length
      const notEnoughUpgraders = harvestersNeeded > upgraders.length
      for (let i = 0; i < (notEnoughUpgraders ? upgraders.length : harvestersNeeded); i++) {
        const creep = upgraders.pop()
        creep.memory.role = Memory.roles.harvester
      }
      if (notEnoughUpgraders) {
        spawnCreep(spawns, Memory.roles.harvester, getParts(room, Memory.roles.harvester))
      }
    }
  } else {
    const { level } = room.controller
    if (defenders.length < level) {
      if (!room.memory.defender) {
        room.memory.defender = Memory.roles.melee
      }
      let currentDeployment = room.memory.currentDeployment || 0
      if (spawnCreep(spawns, Memory.roles.defender, getParts(room, room.memory.defender), {
        defending: currentDeployment,
        capability: room.memory.defender
      }) === OK) {
        currentDeployment++
        if (currentDeployment === (room.memory.sources || []).length) {
          currentDeployment = 0
        }
        room.memory.currentDeployment = currentDeployment
        // TODO: switch defender parts
      }
    } else if (builders.length < level + 1) {
      spawnCreep(spawns, Memory.roles.builder, getParts(room, Memory.roles.builder))
    } else if (upgraders.length < level) {
      spawnCreep(spawns, Memory.roles.upgrader, getParts(room, Memory.roles.upgrader))
    }
  }

  // map creeps
  const hostile = room.find(FIND_HOSTILE_CREEPS).map(hostile => hostile.id) || []
  room.memory.hostile = hostile

  if (hostile.length > 0 && room.memory.fightEnded) {
    room.memory.fightEnded = false
    for (const name in Game.creeps) {
      Game.creeps[name].memory.role = Memory.roles.defender
    }
  } else if (!room.memory.fightEnded) {
    room.memory.fightEnded = true
    for (const name in Game.creeps) {
      const creep = Game.creeps[name]
      const { sourceId, building, upgrading } = creep.memory
      if (sourceId) {
        creep.memory.role = Memory.roles.harvester
      } else if (building) {
        creep.memory.role = Memory.roles.builder
      } else if (upgrading) {
        creep.memory.role = Memory.roles.upgrader
      }
    }
  }
}

module.exports = manageCreeps
