const { prepareRoom, prepareWorld } = require('./prepare')
const manageCreeps = require('./strategy.creep')
const manageStructures = require('./strategy.structure')

module.exports.loop = () => {
  prepareWorld()

  for (const roomId in Game.rooms) {
    const room = Game.rooms[roomId]

    if (!(room.controller && room.controller.my)) {
      continue
    }

    const spawns = room.find(FIND_MY_SPAWNS)
    const terrain = room.getTerrain()

    prepareRoom({ room, spawns, terrain })
    manageStructures({ room, spawns, terrain })
    manageCreeps({ room, spawns })

  }
}
