const { getSurroundingPositions, invalidTerrain } = require('./utility')

const createPathsFromSpawns = (room, spawns, x, y) => {
  let success = true

  for (const spawn of spawns) {
    const positions = spawn.pos.findPathTo(x, y, {
      ignoreRoads: true,
      ignoreCreeps: true,
      swampCost: 1
    })

    for (const { x, y } of positions) {
      const status = room.createConstructionSite(STRUCTURE_ROAD, x, y)
      if (![OK, ERR_INVALID_TARGET].includes(status)) {
        success = false
        break
      }
    }
  }

  return success
}

const mapSources = ({ room, spawns, terrain }) => {
  const sources = room.find(FIND_SOURCES)

  //x, y
  const sourceData = {}

  let harvestersForRoom = 0

  for (const source of sources) {
    const { x, y } = source.pos
    let maxHarvesters = 0

    // p for position
    for (const [adjacentX, adjacentY] of getSurroundingPositions(x, y)) {
      if (!invalidTerrain(terrain, adjacentX, adjacentY)) {
        // construction sites for road from spawn to source
        createPathsFromSpawns(room, spawns, adjacentX, adjacentY)
        maxHarvesters++
        harvestersForRoom++
      }
    }

    // map coordinates to patrol for defenders
    // defenders should hangout around sources
    const patrolLine = getSurroundingPositions(x, y, 4).filter(([pX, pY]) => !invalidTerrain(terrain, pX, pY))

    // restore source-creep mapping
    const sourceIds = _.filter(Game.creeps, creep => creep.memory.sourceId === source.id)

    maxHarvesters++
    harvestersForRoom++
    sourceData[source.id] = {
      maxHarvesters,
      patrolLine,
      harvesters: sourceIds.length
    }
  }

  room.memory.sources = sourceData
  room.memory.maxHarvesters = harvestersForRoom
  room.memory.sourceMapped = true
}

const mapControllers = ({ room, spawns, terrain }) => {
  const { x, y } = room.controller.pos

  // p for position
  for (const [adjacentX, adjacentY] of getSurroundingPositions(x, y)) {
    if (terrain.get(adjacentX, adjacentY) !== TERRAIN_MASK_WALL) {
      // construction sites for road from spawn to source
      if (!createPathsFromSpawns(room, spawns, adjacentX, adjacentY)) {
        return
      }
    }
  }

  room.memory.controllerMapped = true
}

const mapExits = ({ room }) => {
  const exits = room.find(FIND_EXIT)
  const roomExits = {
    border: {
      top: {},
      right: {},
      bottom: {},
      left: {}
    },
    patrol: {
      top: {},
      right: {},
      bottom: {},
      left: {}
    }
  }

  for (const exit of exits) { // ignore corner exits
    const { x, y } = exit
    const key = `${x},${y}`

    const defenderCount = {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0
    }

    if (x !== 0 && x !== 49) {
      if (y === 0) { // top
        roomExits.border.top[key] = defenderCount.top
        roomExits.patrol.top[`${x},${y + 1}`] = defenderCount.top
      } else if (y === 49) { // bottom
        roomExits.border.bottom[key] = defenderCount.bottom
        roomExits.patrol.bottom[`${x},${y - 1}`] = defenderCount.bottom
      }
    } else if (y !== 49 && y !== 0) {
      if (x === 0) { // left
        roomExits.border.left[key] = defenderCount.left
        roomExits.patrol.left[`${x + 1},${y}`] = defenderCount.left
      } else if (x === 49) { // right
        roomExits.border.right[key] = defenderCount.right
        roomExits.patrol.right[`${x - 1},${y}`] = defenderCount.right
      }
    }
  }

  roomExits.border.top.array = Object.keys(roomExits.border.top).sort()
  roomExits.border.right.array = Object.keys(roomExits.border.right).sort()
  roomExits.border.bottom.array = Object.keys(roomExits.border.bottom).sort()
  roomExits.border.left.array = Object.keys(roomExits.border.left).sort()
  roomExits.patrol.top.array = Object.keys(roomExits.patrol.top).sort()
  roomExits.patrol.right.array = Object.keys(roomExits.patrol.right).sort()
  roomExits.patrol.bottom.array = Object.keys(roomExits.patrol.bottom).sort()
  roomExits.patrol.left.array = Object.keys(roomExits.patrol.left).sort()
  room.memory.exits = roomExits
  room.memory.exitMapped = true
}

const mapExtensions = ({ room }) => {
  room.memory.extensions = room.find(FIND_MY_STRUCTURES, {
    filter: s => s.structureType === STRUCTURE_EXTENSION
  }).map(s => s.id)
  room.memory.extensionMapped = true
}

const mapDefenders = ({ room }) => {
  room.memory.currentDeployment = 0
  room.memory.defenderMapped = true
}

const prepareRoom = ({ room, spawns, terrain }) => {
  const { sourceMapped, controllerMapped, exitMapped, roads, extensionMapped, defenderMapped } = room.memory

  if (!roads) {
    const roadMap = {}

    const roadArray = room.find(FIND_STRUCTURES, {
      filter: s => s.structureType === STRUCTURE_ROAD
    })

    for (const road of roadArray) {
      roadMap[`${road.pos.x},${road.pos.y}`] = 1
    }

    room.memory.roads = roadMap
  }

  if (!sourceMapped) {
    mapSources({ room, spawns, terrain })
  }

  if (!controllerMapped) {
    mapControllers({ room, spawns, terrain })
  }

  if (!exitMapped) {
    mapExits({ room })
  }

  if (!extensionMapped) {
    mapExtensions({ room })
  }

  if (!defenderMapped) {
    mapDefenders({ room })
  }
}

const prepareWorld = () => {
  // BODYPART_COST: {
  //   "move": 50,
  //   "work": 100,
  //   "attack": 80,
  //   "carry": 50,
  //   "heal": 250,
  //   "ranged_attack": 150,
  //   "tough": 10,
  //   "claim": 600
  // }

  Memory.parts = {
    harvester: {
      // controller at level 6 or below
      300: [WORK, CARRY, MOVE, MOVE], // default
      350: [WORK, WORK, CARRY, MOVE, MOVE, MOVE], // 1 extension
      450: [WORK, WORK, CARRY, MOVE, MOVE, MOVE], // 3 extensions
      600: [WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE, MOVE], // 6 extensions
      800: [RANGED_ATTACK, WORK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE] // 10 extensions
    },

    builder: {
      // controller at level 6 or below
      300: [WORK, CARRY, MOVE, MOVE], // default
      350: [WORK, WORK, CARRY, MOVE, MOVE, MOVE], // 1 extension
      450: [WORK, WORK, CARRY, MOVE, MOVE, MOVE], // 3 extensions
      600: [WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE, MOVE], // 6 extensions
      800: [RANGED_ATTACK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE] // 10 extensions
    },

    upgrader: {
      // controller at level 6 or below
      300: [WORK, CARRY, MOVE, MOVE], // default
      350: [WORK, WORK, CARRY, MOVE, MOVE, MOVE], // 1 extension
      450: [WORK, WORK, CARRY, MOVE, MOVE, MOVE], // 3 extensions
      600: [WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE, MOVE], // 6 extensions
      800: [RANGED_ATTACK, WORK, WORK, WORK, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE] // 10 extensions
    },

    melee: {
      300: [ATTACK, TOUGH, TOUGH, MOVE, MOVE, MOVE, MOVE],
      450: [ATTACK, ATTACK, ATTACK, TOUGH, MOVE, MOVE, MOVE, MOVE]
    },

    ranged: {
      300: [RANGED_ATTACK, MOVE, MOVE, MOVE],
      450: [RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE, MOVE]
    },

    healer: {
      400: [HEAL, MOVE, MOVE, MOVE],
      650: [HEAL, HEAL, MOVE, MOVE, MOVE]
    }
  }

  Memory.roles = {
    harvester: 'harvester',
    builder: 'builder',
    upgrader: 'upgrader',
    defender: 'defender',

    // battle unit roles
    melee: 'melee',
    ranged: 'ranged',
    heal: 'healer'
  }

  // index 0 = level 2 controller
  Memory.extensionCount = [5, 10, 20, 30, 40, 50, 60]
  Memory.extensionCapacity = [50, 50, 50, 50, 50, 100, 200]

  // index 0 = level 3 controller
  Memory.towerCount = [1, 1, 2, 2, 3, 6]

  Memory.initialized = true
}

module.exports = {
  prepareRoom,
  prepareWorld
}
