const use = creep => {
  if (!Game.creeps[creep.name]) {
    return
  }

  if (creep.memory.upgrading && creep.store[RESOURCE_ENERGY] === 0) {
    creep.memory.upgrading = false
  } else if (!creep.memory.upgrading && creep.store[RESOURCE_ENERGY] === creep.store.getCapacity(RESOURCE_ENERGY)) {
    creep.memory.upgrading = true
  }

  if (creep.memory.upgrading) {
    // go upgrade controller
    if (creep.upgradeController(creep.room.controller) === ERR_NOT_IN_RANGE) {
      creep.moveTo(creep.room.controller, {
        visualizePathStyle: {
          stroke: '#ffaa00'
        }
      })
    }
  } else {
    // go get some energy
    let structure = creep.pos.findClosestByRange(FIND_TOMBSTONES, {
      filter: s => s.store && s.store[RESOURCE_ENERGY] > 0
    })

    if (!structure) {
      structure = creep.pos.findClosestByRange(FIND_MY_STRUCTURES, {
        filter: s => s.store && s.store[RESOURCE_ENERGY] > 0 && [
          STRUCTURE_SPAWN,
          STRUCTURE_EXTENSION,
          STRUCTURE_CONTAINER
        ].includes(s.structureType)
      })
    }

    if (structure && creep.withdraw(structure, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
      creep.moveTo(structure, { visualizePathStyle: { stroke: '#ffffff' } })
    }
  }
}

module.exports = {
  use
}