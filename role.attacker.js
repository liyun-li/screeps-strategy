var varDefault = require('var.default');

module.exports = {
    run: function(creep) {
        var roomId = varDefault.roomId;
        var roomObj = Game.rooms[roomId];
        
        var victimId = varDefault.victimId;
        var victimObj = Game.rooms[victimId];
        
        var structs = creep.room.find(FIND_HOSTILE_STRUCTURES);
        
        var attackers = _.filter(Game.creeps, (creep) => creep.memory.role == 'attacker');
        
        var x = varDefault.gatherX;
        var y = varDefault.gatherY;
        
        var x2 = varDefault.victimGatherX;
        var y2 = varDefault.victimGatherY;
        
        var enemy = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        var struct = creep.pos.findClosestByRange(FIND_HOSTILE_STRUCTURES);
        
        if (attackers.length < 6 && creep.room == roomObj) {
            if (!enemy) {
                creep.moveTo(x, y, {visualizePathStyle: {stroke: '#ffaa00'}});
            } else if (creep.attack(enemy) == ERR_NOT_IN_RANGE) {
                creep.moveTo(enemy, {visualizePathStyle: {stroke: '#ffaa00'}});
            }
        } else if (creep.room != victimObj) {
            creep.moveTo(new RoomPosition(x2, y2, victimId), {visualizePathStyle: {stroke: '#ffaa00'}});
        } else {
            var towerIndex = -1;
            for (var i in structs) {
                if (structs[i].toString().toLowerCase().includes('tower')) {
                    towerIndex = i;
                    break;
                }
            }
            
            if (towerIndex > -1) {
                if (creep.attack(structs[i]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(structs[i], {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            } else if (enemy) {
                if (creep.attack(enemy) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(enemy, {visualizePathStyle: {stroke: '#ffaa00'}});
                }
            } else if (struct) {
                for (var i in structs) {
                    if (!structs[i].toString().includes('controller')) {
                        if (creep.attack(structs[i]) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(structs[i], {visualizePathStyle: {stroke: '#ffaa00'}});
                        }
                        break;
                    }
                }
            } else if (creep.attack(struct) == ERR_NOT_IN_RANGE) {
                creep.moveTo(struct, {visualizePathStyle: {stroke: '#ffaa00'}});
            }
            creep.say('FRIENDLY');
        }
        
    }
};