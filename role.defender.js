const use = creep => {
  if (!Game.creeps[creep.name]) {
    return
  }

  if ((creep.room.memory.hostile || []).length > 0) {
    const hostile = creep.room.memory.hostile.map(id => Game.getObjectById(id))
    const target = creep.pos.findClosestByPath(hostile)
    if (creep.memory.capability === Memory.roles.melee) {
      if (creep.attack(target) === ERR_NOT_IN_RANGE) {
        creep.moveTo(target)
      }
    } else if (creep.memory.capability === Memory.roles.ranged) {
      if (creep.rangedAttack(target) === ERR_NOT_IN_RANGE) {
        creep.moveTo(target)
      }
    }
  } else {
    const { sources } = creep.room.memory
    const { defending } = creep.memory
    let { x, y } = creep.pos

    let patrolIndex = creep.memory.patrolIndex || 0

    const patrolLine = sources[Object.keys(sources)[defending || 0]].patrolLine
    if (patrolLine.map(([x, y]) => `${x},${y}`).includes(`${x},${y}`)) {
      patrolIndex++
      if (patrolIndex === patrolLine.length) {
        patrolIndex = 0
      }
    }

    let [goalX, goalY] = patrolLine[patrolIndex]

    goalX = parseInt(goalX)
    goalY = parseInt(goalY)

    creep.moveTo(goalX, goalY, { visualizePathStyle: { stroke: '#ffaa00' } })
    creep.memory.patrolIndex = patrolIndex

    if (patrolIndex === patrolLine.length) {
      creep.memory.patrolIndex = 0
    }
  }
}

module.exports = {
  use
}
