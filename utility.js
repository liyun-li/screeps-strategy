/**
 * get the surrounding positions
 * @param {Number} x value of X
 * @param {Number} y value of Y 
 * @param {Number} distance distance between the center and the positions
 * @returns {Array} an array of the surrounding positions
 */
const getSurroundingPositions = (x, y, distance = 1) => {
  const positions = []

  if (y - distance > 0) {
    for (let _x = x - distance; _x < x + distance + 1; _x++) {
      if (_x > 0 && _x < 49) {
        positions.push([_x, y - distance])
      }
    }
  }

  if (y + distance < 49) {
    for (let _x = x - distance; _x <= x + distance + 1; _x++) {
      if (_x > 0 && _x < 49) {
        positions.push([_x, y + distance])
      }
    }
  }

  if (x - distance > 0) {
    for (let _y = y - distance + 1; _y < y + distance; _y++) {
      if (_y > 0 && _y < 49) {
        positions.push([x - distance, _y])
      }
    }
  }

  if (x + distance < 49) {
    for (let _y = y - distance + 1; _y < y + distance; _y++) {
      if (_y > 0 && _y < 49) {
        positions.push([x + distance, _y])
      }
    }
  }

  return positions
}

/**
 * spawn a creep
 * @param {Array} spawns Array of spawns to spawn the creep
 * @param {string} role creep role
 * @param {Array} parts creep parts
 */
const spawnCreep = (spawns, role, parts, memory = {}) => {
  spawns = spawns.filter(spawn => !spawn.spawning)

  if (spawns.length > 0) {
    const spawn = spawns[0]
    const name = `${spawn.name}-${role}-${Game.time}`
    const extensions = (spawn.room.memory.extensions || []).map(id => Game.getObjectById(id))
    const result = spawn.spawnCreep(parts, name, {
      memory: {
        ...memory,
        role
      },
      energyStructures: [
        ...extensions,
        spawn
      ]
    })
    if (![OK, ERR_NOT_ENOUGH_ENERGY].includes(result)) {
      console.log(`Error spawning ${role} ${name}: ${result}`)
    }
  }
}

const invalidTerrain = (terrain, x, y, condition = [TERRAIN_MASK_WALL]) =>
  condition.includes(terrain.get(x, y))

/**
* Get parts based on the room capacity
* @param {Room} room Room object
* @param {string} type choose from one of the creep roles
*/
const getParts = (room, type) => {
  let roomLevel = room.energyCapacityAvailable
  const roomLevels = Object.keys(Memory.parts[type]).sort((a, b) => a > b)

  for (const level of roomLevels) {
    if (room.energyCapacityAvailable >= level) {
      roomLevel = level
    } else {
      break
    }
  }

  return Memory.parts[type][roomLevel]
}

module.exports = {
  getSurroundingPositions,
  spawnCreep,
  getParts,
  invalidTerrain
}
